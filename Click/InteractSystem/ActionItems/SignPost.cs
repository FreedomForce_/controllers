﻿using Lasika.Dialogue;
using UnityEngine;

namespace Lasika.Controllers.PAC.InteractionSystem.ActionItem {
    public class SignPost : ActionItem {
        public string[] Dialogue;

        public override void Interact() {
            DialogueSystem.Instance.AddNewDialogue(Dialogue, "Old Sign");
            Debug.Log("SignPost::Interact -> Interacted with SignPost class");
        }
    }
}