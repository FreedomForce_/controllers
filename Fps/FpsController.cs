﻿using System;
using Lasika.Utility;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Lasika.Controllers.FPS {
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Controls))]
    [RequireComponent(typeof(Player))]
    public class FpsController : MonoBehaviour {
        public bool IsWalking;
        public float WalkSpeed = 5;
        public float RunSpeed = 10;
        [Range(0f, 1f)] public float RunStepLength = 0.7f;

        public float JumpSpeed = 1;
        public float StickToGroundForce = 10;
        public float GravityMultiplier = 2;
        public MouseLook MouseLook;
        public float StepInterval = 5;
        public AudioClip[] FootstepSounds;

        private Player _player;
        private Camera _camera;
        private bool _jump;
        private Vector2 _input;
        private Vector3 _moveDir = Vector3.zero;
        private CharacterController _characterController;
        private CollisionFlags _collisionFlags;
        private bool _previouslyGrounded;
        private AudioSource _audioSource;
        private float _stepCycle;

        private float _nextStep;
        // private EventMapper.EventMapper _eventMapper;


        // Use this for initialization
        private void Start() {
            _characterController = GetComponent<CharacterController>();
            _audioSource = GetComponent<AudioSource>();
            _camera = Camera.main;
            _stepCycle = 0f;
            _nextStep = _stepCycle / 2f;
            MouseLook = new MouseLook();
            MouseLook.Init(transform, _camera != null ? _camera.transform : null);
            //_eventMapper = GetComponent<EventMapper.EventMapper>();
            _player = GetComponent<Player>();
        }

        // Update is called once per frame
        private void Update() {
            if (_player.IsDead)
                return;
            RotateView();
            // the jump state needs to read here to make sure it is not missed
            if (!_jump) {
                _jump = Controls.IsKeyPressed("Jump");
            }

            if (!_previouslyGrounded && _characterController.isGrounded) {
                Landing();
                _moveDir.y = 0f;
            }


            if (Controls.IsKeyPressed("Eat")) {
                throw new NotImplementedException();
            }

            if (Controls.IsKeyPressed("Drink")) {
                throw new NotImplementedException();
            }

            _previouslyGrounded = _characterController.isGrounded;
        }


        private void FixedUpdate() {
            if (_player.IsDead)
                return;
            float speed;
            GetInput(out speed);
            Vector3 desiredMove = transform.forward * _input.y + transform.right * _input.x;
            if ((int) _input.x != 0 || (int) _input.y != 0) {
                // if (_eventMapper != null)
                //     _eventMapper.CallEvent("Move");
            } else if ((int) _input.x == 0 || (int) _input.y == 0) {
                //  if (_eventMapper != null)
                //     _eventMapper.CallEvent("Stop");
            }

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, _characterController.radius, Vector3.down, out hitInfo,
                _characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;


            if (_characterController.isGrounded) {
                _moveDir.y = -StickToGroundForce;

                _moveDir.x = desiredMove.x * speed;
                _moveDir.z = desiredMove.z * speed;
                if (_jump) {
                    //  if (_eventMapper != null)
                    //      _eventMapper.CallEvent("Jump");
                    _jump = false;
                }
            } else {
                _moveDir += Physics.gravity * GravityMultiplier * Time.fixedDeltaTime;
            }

            _collisionFlags = _characterController.Move(_moveDir * Time.fixedDeltaTime);

            ProgressStepCycle(speed);

            MouseLook.UpdateCursorLock();
        }


        private void Landing() {
            //    if (_eventMapper != null)
            //      _eventMapper.CallEvent("Land");
            _nextStep = _stepCycle + .5f;
        }

        public void Jump() {
            // ReSharper disable once ObjectCreationAsStatement
            new WaitForSeconds(1);
            _moveDir.y = JumpSpeed;
        }

        private void ProgressStepCycle(float speed) {
            if (_characterController.velocity.sqrMagnitude > 0 && ((int) _input.x != 0 || (int) _input.y != 0)) {
                _stepCycle += (_characterController.velocity.magnitude + (speed * (IsWalking ? 1f : RunStepLength))) *
                              Time.fixedDeltaTime;
            }

            if (!(_stepCycle > _nextStep)) {
                return;
            }

            _nextStep = _nextStep + StepInterval;

            PlayFootStepAudio();
        }

        private void PlayFootStepAudio() {
            if (!_characterController.isGrounded) {
                return;
            }


            if (FootstepSounds.Length == 1) {
                float n = Random.Range(1f, 1.8f);
                _audioSource.clip = FootstepSounds[0];
                _audioSource.pitch = n;
                _audioSource.Play();
            } else if (FootstepSounds.Length > 1) {
                int n = Random.Range(1, FootstepSounds.Length);
                _audioSource.clip = FootstepSounds[n];
                _audioSource.PlayOneShot(_audioSource.clip);

                FootstepSounds[n] = FootstepSounds[0];
                FootstepSounds[0] = _audioSource.clip;
            }
        }

        private void GetInput(out float speed) {
            float vertical = 0;
            float horizontal = 0;
            if (Controls.IsKeyHold("Forward"))
                vertical = 1;
            if (Controls.IsKeyHold("Backwards"))
                vertical = -1;
            if (Controls.IsKeyHold("Left"))
                horizontal = -1;
            if (Controls.IsKeyHold("Right"))
                horizontal = 1;
            //float vertical = Input.GetAxis("Vertical");
            //float horizontal = Input.GetAxis("Horizontal");

            IsWalking = !Input.GetKey(KeyCode.LeftShift);
            speed = IsWalking ? WalkSpeed : RunSpeed;
            _input = new Vector2(horizontal, vertical);
            if (_input.sqrMagnitude > 1) {
                _input.Normalize();
            }
        }

        private void RotateView() {
            MouseLook?.LookRotation(transform, _camera.transform);
        }

        private void OnControllerColliderHit(ControllerColliderHit hit) {
            Rigidbody body = hit.collider.attachedRigidbody;
            if (_collisionFlags == CollisionFlags.Below) {
                return;
            }

            if (body == null || body.isKinematic) {
                return;
            }

            body.AddForceAtPosition(_characterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
        }
    }
}