﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lasika.Achievements;
using Lasika.Utility;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Lasika.Controllers {
    public class Achievements : MonoBehaviour {
        public GameObject AchievementGameObject;

        public Transform AchievementParent;

        public TMP_Text CompletedAchievements;

        private int _completedAchievements;
        public TMP_Text TotalPoints;
        private int _totalPoints;
        private Dictionary<GameObject, Achievement> _achievementGameObjects;

        private void Awake() {
            _achievementGameObjects = new Dictionary<GameObject, Achievement>();
            Achievement.LoadAchievements().Values.ToList().ForEach(achievement => {
                GameObject go = Instantiate(AchievementGameObject, AchievementParent);
                AssignValues(go, achievement);
                _achievementGameObjects.Add(go, achievement);
            });
            CountCompleted();
        }

        // Update is called once per frame
        private void Update() {
            if (!Achievement.HasUpdated) return;
            foreach (GameObject go in _achievementGameObjects.Keys) {
                Achievement achievement = _achievementGameObjects[go];
                AssignValues(go, achievement);
            }

            CountCompleted();
            Achievement.HasUpdated = false;
        }

        private void OnApplicationQuit() {
            JsonUtil.SaveItem(Achievement.Achievements, "Achievements/User/Achievements", debug: true);
        }

        private void CountCompleted() {
            _totalPoints = 0;
            _completedAchievements = 0;
            foreach (Achievement ah in _achievementGameObjects.Values) {
                if (!ah.Completed) continue;
                _totalPoints += ah.Points;
                _completedAchievements++;
            }

            TotalPoints.text = " " + _totalPoints;
            CompletedAchievements.text = " " + _completedAchievements + "/" + Achievement.Achievements.Count;
        }

        private void AssignValues(GameObject go, Achievement achievement) {
            if (achievement.Hidden && !achievement.Completed) {
                go.transform.Find("Title").GetComponentInChildren<TMP_Text>().text = "?";
                go.transform.Find("Description").GetComponentInChildren<TMP_Text>().text = "Revealed when unlocked";
                go.transform.Find("Points").GetComponent<TMP_Text>().text = "Points: " + achievement.Points;
                go.transform.Find("Accomplished").GetComponent<TMP_Text>().text = achievement.PercentCompleted + "%";
                //TODO: Broken not loading correctly
                go.transform.Find("Image").GetComponent<Image>().sprite =
                    ResourceLoader.LoadSprite("Achievements/Unknown");
                return;
            }

            go.transform.Find("Title").GetComponentInChildren<TMP_Text>().text = achievement.Title;

            if (achievement.Completed)
                go.transform.Find("Description").GetComponentInChildren<TMP_Text>().text =
                    achievement.AchievedDescription;
            else
                go.transform.Find("Description").GetComponentInChildren<TMP_Text>().text =
                    achievement.UnachievedDescription;
            //TODO: Broken not loading correctly
            go.transform.Find("Image").GetComponent<Image>().sprite =
                ResourceLoader.LoadSprite("Achievements/" + achievement.Image);
            go.transform.Find("Points").GetComponent<TMP_Text>().text = "Points: " + achievement.Points;
            // go.transform.Find("Percent").GetComponent<TMP_Text>().text = achievement.PercentCompleted.ToString(CultureInfo.CurrentCulture) + "%";
            if (achievement.Completed)
                go.transform.Find("Accomplished").GetComponent<TMP_Text>().text =
                    achievement.CompeteDate.ToString(CultureInfo.CurrentCulture);
            else
                go.transform.Find("Accomplished").GetComponent<TMP_Text>().text =
                    achievement.PercentCompleted.ToString(CultureInfo.CurrentCulture) + "%";
        }
    }
}